package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {

        public static String getMontoEscrito(int valor){

            String cadena = new String();

        //Identificar billones
        if ((valor / 100000000)>0){
            if ((valor / 100000000) == 1){
                cadena = "cien millones ";
            }
            else {
                cadena = getMontoEscrito(valor / 100000000) + "millones " + getMontoEscrito(valor % 100000000);
            }
        }
        else {
            //Identifica si lleva millones.
            if ((valor / 1000000) > 0) {
                if ((valor / 1000000) == 1) {
                    cadena = "un millon ";
                } else {
                    cadena = getMontoEscrito(valor / 1000000) + "millones " + getMontoEscrito(valor % 1000000);
                }

            } else {
                //Identifica si tiene miles.
                if ((valor / 1000) > 0) {
                    if ((valor / 1000) == 1) {
                        cadena = "mil ";
                    } else {
                        cadena = getMontoEscrito(valor / 1000) + "mil " + getMontoEscrito(valor % 1000);
                    }
                } else {
                    //Identifica si lleva cientos.
                    if ((valor / 100) > 0) {
                        if ((valor / 100) == 1) {
                            if ((valor % 100) == 0) {
                                cadena = "cien ";
                            } else {
                                cadena = "ciento " + getMontoEscrito(valor % 100);
                            }
                        } else {
                            if ((valor / 100) == 2) {
                                cadena = "doscientos " + getMontoEscrito(valor % 100);
                            } else {
                                if ((valor / 100) == 3) {
                                    cadena = "trescientos " + getMontoEscrito(valor % 100);
                                } else {
                                    if ((valor / 100) == 4) {
                                        cadena = "cuatrocientos " + getMontoEscrito(valor % 100);
                                    } else {
                                        if ((valor / 100) == 5) {
                                            cadena = "quinientos " + getMontoEscrito(valor % 100);
                                        } else {
                                            if ((valor / 100) == 6) {
                                                cadena = "seiscientos " + getMontoEscrito(valor % 100);
                                            } else {
                                                if ((valor / 100) == 7) {
                                                    cadena = "setecientos " + getMontoEscrito(valor % 100);
                                                } else {
                                                    if ((valor / 100) == 8) {
                                                        cadena = "ochocientos " + getMontoEscrito(valor % 100);
                                                    } else {
                                                        if ((valor / 100) == 9) {
                                                            cadena = "novecientos " + getMontoEscrito(valor % 100);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //cadena = getMontoEscrito(valor / 100) + "cientos " + getMontoEscrito(valor % 100);
                                    }
                                }
                            }
                        }
                    }
                    //Identifica si hay decenas.
                    else {
                        if ((valor / 10) > 0) {
                            switch ((int) (valor / 10)) {
                                case 1:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "diez ";
                                            break;
                                        case 1:
                                            cadena = "once ";
                                            break;
                                        case 2:
                                            cadena = "doce ";
                                            break;
                                        case 3:
                                            cadena = "trece ";
                                            break;
                                        case 4:
                                            cadena = "catorce ";
                                            break;
                                        case 5:
                                            cadena = "quince ";
                                            break;
                                        default:
                                            cadena = "diez y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 2:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "veinte ";
                                            break;
                                        default:
                                            cadena = "veinti " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 3:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "treinta ";
                                            break;
                                        default:
                                            cadena = "treinta y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 4:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "cuarenta ";
                                            break;
                                        default:
                                            cadena = "cuarenta y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 5:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "cincuenta ";
                                            break;
                                        default:
                                            cadena = "cincuenta y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 6:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "sesenta ";
                                            break;
                                        default:
                                            cadena = "sesenta y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 7:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "setenta ";
                                            break;
                                        default:
                                            cadena = "setenta y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 8:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "ochenta ";
                                            break;
                                        default:
                                            cadena = "ochenta y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                                case 9:
                                    switch ((int) (valor % 10)) {
                                        case 0:
                                            cadena = "noventa ";
                                            break;
                                        default:
                                            cadena = "noventa y " + getMontoEscrito(valor % 10);
                                            break;
                                    }
                                    break;
                            }
                        } else {
                            switch ((int) (valor)) {
                                case 0:
                                    cadena = "cero ";
                                    break;
                                case 1:
                                    cadena = "uno ";
                                    break;
                                case 2:
                                    cadena = "dos ";
                                    break;
                                case 3:
                                    cadena = "tres ";
                                    break;
                                case 4:
                                    cadena = "cuatro ";
                                    break;
                                case 5:
                                    cadena = "cinco ";
                                    break;
                                case 6:
                                    cadena = "seis ";
                                    break;
                                case 7:
                                    cadena = "siete ";
                                    break;
                                case 8:
                                    cadena = "ocho ";
                                    break;
                                case 9:
                                    cadena = "nueve ";
                                    break;
                            }
                        }
                    }
                }

            }
        }


        return cadena;
    }

        /*private static void crearParseNumeros() {

            parseoNumeros.put(0, "cero");
            parseoNumeros.put(1, "uno");
            parseoNumeros.put(2, "dos");
            parseoNumeros.put(3, "tres");
            parseoNumeros.put(4, "cuatro");
            parseoNumeros.put(5, "cinco");
            parseoNumeros.put(6, "seis");
            parseoNumeros.put(7, "siete");
            parseoNumeros.put(8, "ocho");
            parseoNumeros.put(9, "nueve");
            parseoNumeros.put(10, "diez");
            parseoNumeros.put(11, "once");
            parseoNumeros.put(12, "doce");
            parseoNumeros.put(13, "trece");
            parseoNumeros.put(14, "catorce");
            parseoNumeros.put(15, "quince");
            parseoNumeros.put(20, "veinte");
            parseoNumeros.put(30, "treinta");
            parseoNumeros.put(40, "cuarenta");
            parseoNumeros.put(50, "cincuenta");
            parseoNumeros.put(60, "sesenta");
            parseoNumeros.put(70, "setenta");
            parseoNumeros.put(80, "ochenta");
            parseoNumeros.put(90, "noventa");
            parseoNumeros.put(100, "cien");
            parseoNumeros.put(500, "quinientos");
            parseoNumeros.put(900, "novecientos");
            parseoNumeros.put(1000, "mil");
            parseoNumeros.put(1000000, "un millon");
        }*/
    }

